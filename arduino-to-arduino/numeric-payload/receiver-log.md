# RX log

```bash
# is this device a sender (TX) or a receiver (RX)?
```

Press `RX` on the Serial Monitor.

```bash
This device is RX

# Change behaviour: press 'T' to begin transmitting to the other node
### start debugging info ###
SPI Frequency = 10 Mhz
Channel = 76 (~ 2476 MHz)
RF Data Rate = 1 MBPS
RF Power Amplifier = PA_LOW
RF Low Noise Amplifier = Enabled
CRC Length = 16 bits
Address Length = 5 bytes
Static Payload Length = 4 bytes
Auto Retry Delay = 1500 microseconds
Auto Retry Attempts = 15 maximum
Packets lost on current channel = 0
Retry attempts made for last transmission = 0
Multicast = Disabled
Custom ACK Payload = Disabled
Dynamic Payloads = Disabled
Auto Acknowledgment = Enabled
Primary Mode = RX
TX address = 0x65646f4e31
pipe 0 (closed) bound = 0x65646f4e31
pipe 1 ( open ) bound = 0x65646f4e32
pipe 2 (closed) bound = 0xc3
pipe 3 (closed) bound = 0xc4
pipe 4 (closed) bound = 0xc5
pipe 5 (closed) bound = 0xc6
### end debugging info ###

Received 4 bytes on pipe 1: 0.00
Received 4 bytes on pipe 1: 0.01
Received 4 bytes on pipe 1: 0.02
Received 4 bytes on pipe 1: 0.03
Received 4 bytes on pipe 1: 0.04
Received 4 bytes on pipe 1: 0.05
Received 4 bytes on pipe 1: 0.06
Received 4 bytes on pipe 1: 0.07
Received 4 bytes on pipe 1: 0.08
Received 4 bytes on pipe 1: 0.09
Received 4 bytes on pipe 1: 0.10
Received 4 bytes on pipe 1: 0.11
```

Press `T` on the Serial Monitor.

```bash
# Change to Trasmit Role (TX) ~ press 'R' to switch back
Transmission successful! Time to transmit = 564 us. Sent: 0.11
Transmission successful! Time to transmit = 552 us. Sent: 0.12
Transmission successful! Time to transmit = 552 us. Sent: 0.13
Transmission successful! Time to transmit = 552 us. Sent: 0.14
Transmission successful! Time to transmit = 552 us. Sent: 0.15
Transmission successful! Time to transmit = 552 us. Sent: 0.16
Transmission successful! Time to transmit = 552 us. Sent: 0.17
Transmission successful! Time to transmit = 560 us. Sent: 0.18
Transmission successful! Time to transmit = 556 us. Sent: 0.19
Transmission successful! Time to transmit = 552 us. Sent: 0.15
Transmission successful! Time to transmit = 552 us. Sent: 0.16
Transmission successful! Time to transmit = 552 us. Sent: 0.17
Transmission successful! Time to transmit = 560 us. Sent: 0.18
Transmission successful! Time to transmit = 556 us. Sent: 0.19
Transmission successful! Time to transmit = 560 us. Sent: 0.20
```
