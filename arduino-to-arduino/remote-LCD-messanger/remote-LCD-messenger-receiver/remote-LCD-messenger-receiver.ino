/*
   Remote LCD Messender - Receiver
   Receive the message using the nRF24L01 wireless module from the sender and display it on the Liquid Crystal Display (LCD).

   Docs:
   1. https://github.com/nRF24/RF24
   2. https://docs.arduino.cc/learn/electronics/lcd-displays

   Tutorial: https://pietropoluzzi.it/blog/hardware/wireless-communication-nrf24l01-module/
*/
#include <SPI.h>
#include "RF24.h"
#include "printf.h"
#include <LiquidCrystal.h>

// instantiate an object for the nRF24L01 transceiver
// pin 09 for CE and pin 10 for CSN
RF24 radio(9, 10);

const byte address[6] = "00001";

// this value will be read from the wireless communication and will be shown on the LCD
char payload = "";

// initialize LiquidCrystal library by associating any needed LCD
// interface pin with the arduino pin number it is connected to
const int rs = 7, en = 6, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

// define the number of rows and columns of the display
const short int rows = 2, columns = 16;

// support variable to override the messages received
String message = "";

// content displayed on the LCD
String lcd_message = "";

void setup() {
  // initialize serial communication at 9600 baud rate
  Serial.begin(9600);
  // do not wait for serial port to connect.
  // let it works even if the user does not open the Serial Monitor

  // initialize the transceiver on the SPI bus
  if (!radio.begin()) {
    Serial.println("radio hardware is not responding!");
    while (1) {} // hold in infinite loop
  }

  Serial.println("# This device is a sender ~ RX");

  // Set the PA Level low to try preventing power supply related problems since
  // these examples are likely run with nodes in close proximity to each other.
  radio.setPALevel(RF24_PA_LOW); // RF24_PA_MAX is default.

  // set the RX address of the TX node into a RX pipe
  radio.openReadingPipe(0, address);

  // start listening for incoming data
  radio.startListening();

  // debugging info
  Serial.println("### start debugging info ###");
  printf_begin();             // needed only once for printing details
  // radio.printDetails();       // (smaller) function that prints raw register values
  radio.printPrettyDetails(); // (larger) function that prints human readable data
  Serial.println("### end debugging info ###\n");

  // set up the LCD's number of columns and rows
  lcd.begin(columns, rows);

  // clear the screen
  lcd.clear();
  // set the cursor to top left
  lcd.setCursor(0, 0);
}

void loop() {
  radio.startListening();

  // while there is nothing available from the radio, do nothing
  while (!radio.available());

  // read the payload
  radio.read(&payload, sizeof(payload));

  if (message.length() == 1) {
    // clear the screen
    lcd.clear();
    // set the cursor to top left
    lcd.setCursor(0, 0);
  }

  if (payload == '\n') {
    // if the message is complete, use 'lcd_message' variable to display it on the LCD
    // clear the support variable so that it can wait to the next message
    lcd_message = message;
    message = "";

    // if the message is longer than the number of columns
    // it needs to be printed row by row
    if (lcd_message.length() >= columns) {
      // set the indexes that follow the original message
      int start_index = 0;
      int end_index = start_index + columns;

      // loop through the rows and print the message row by row
      // use the support variable 'tmpMessage' to hold the string
      for (int i = 0; i < rows; i++) {
        String tmpMessage = "";

        // 'tmpMessage' length is equal to the number of columns
        for (int j = start_index; j < end_index; j++) {
          // append a char to the support variable
          tmpMessage = tmpMessage + lcd_message.charAt(j);

          // increment the start index to ignore
          // the already "used" chars of the original message
          start_index++;
        }

        // if the message, from the point already saved inside
        // 'tmpMessage' has less than 'columns' more characters
        if (lcd_message.length() < end_index + 16) {
          // set the end index to the message length
          end_index = lcd_message.length();
        } else if (lcd_message.length() > end_index) {
          // if the message, from the point already saved inside
          // 'tmpMessage' has more than 'columns' more characters
          // set the end index to the sum between
          // 'start_index' and the n° of columns
          end_index = start_index + columns;
        }

        // debugging purpose
        Serial.println(tmpMessage);
        
        // set the cursor to the right row
        lcd.setCursor(0, i);
        // print the slice of message
        lcd.print(tmpMessage);
      }
    } else {
      // the message is shorter than the number of columns
      Serial.print("message: ");
      Serial.println(lcd_message);
      lcd.print(lcd_message);
    }
  } else {
    // if the message is not finished, append the character to 'message' variable
    message = message + payload;
    Serial.print("payload: ");
    Serial.println(payload);
  }

  // slow transmissions down by 10 milliseconds
  delay(10);
}
