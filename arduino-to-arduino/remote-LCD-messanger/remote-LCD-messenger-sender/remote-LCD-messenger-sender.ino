/*
   Remote LCD Messenger - Sender

   1. Listen for user input through the Serial Monitor
   2. Send the input to the reciver board using the nRF24L01 wireless module

   Docs: https://github.com/nRF24/RF24
   
   Tutorial: https://pietropoluzzi.it/blog/hardware/wireless-communication-nrf24l01-module/
*/
#include <SPI.h>
#include "RF24.h"
#include "printf.h"

// instantiate an object for the nRF24L01 transceiver
// pin 09 for CE and pin 10 for CSN
RF24 radio(9, 10);

const byte address[6] = "00001";

void setup() {
  // initialize serial communication at 9600 baud rate
  Serial.begin(9600);
  // wait for serial port to connect. Needed for native USB port only.
  while (!Serial)
    ;

  // initialize the transceiver on the SPI bus
  if (!radio.begin()) {
    Serial.println("radio hardware is not responding!");
    while (1) {}  // hold in infinite loop
  }

  Serial.println("# This device is a sender ~ TX");

  // Set the PA Level low to try preventing power supply related problems since
  // these examples are likely run with nodes in close proximity to each other.
  radio.setPALevel(RF24_PA_LOW);  // RF24_PA_MAX is default.

  // set the TX address of the RX node into the TX pipe
  radio.openWritingPipe(address);

  // stop listening for incoming data
  radio.stopListening();

  // debugging info
  Serial.println("### start debugging info ###");
  printf_begin();  // needed only once for printing details
  // radio.printDetails();       // (smaller) function that prints raw register values
  radio.printPrettyDetails();  // (larger) function that prints human readable data
  Serial.println("### end debugging info ###\n");
}

void loop() {
  radio.stopListening();
  String message = "";
  bool report = false;

  // read the input from the Serial Monitor
  while (Serial.available() == 0) {}
  message = Serial.readString();

  // print the message on the Serial Monitor
  Serial.print("message: ");
  Serial.println(message);

  // send the message char by char
  for (int i = 0; i < message.length(); i++) {
    char payload = message.charAt(i);

    report = radio.write(&payload, sizeof(payload));

    if (report) {
      // print payload sent
      Serial.print("position: ");
      Serial.print(i);
      Serial.print("\tpayload: ");
      Serial.println(payload);
    }
  }

  // slow transmissions down by 10 milliseconds
  delay(10);
}
