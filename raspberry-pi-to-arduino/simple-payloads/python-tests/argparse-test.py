import argparse
# https://docs.python.org/3/library/argparse.html

def sender():
    print("I am the sender")

def receiver():
    print("I am the receiver")

parser = argparse.ArgumentParser(description='Choose to be sender or receiver.')
parser.add_argument('-r', '--role', choices=['TX', 'RX'], required=True,
                    help='Select TX to be a sender device. Select RX to be a receiver device.')

args = parser.parse_args()

# prints: 'Namespace(...)' with the arguments inside
print(args)

if args.role == 'TX':
    sender()
elif args.role == 'RX':
    receiver()

# run the code from 'nrf24l01' project folder
# cd raspberry-pi-to-arduino/simple-payloads/python-tests

# python3 argparse-test.py -r TX
# python3 argparse-test.py -r RX
# python3 argparse-test.py --role TX
# python3 argparse-test.py --role RX